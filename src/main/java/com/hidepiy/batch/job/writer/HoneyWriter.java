package com.hidepiy.batch.job.writer;

import org.springframework.batch.item.ItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.hidepiy.batch.domain.dto.Honey;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class HoneyWriter {
    @Bean
    public ItemWriter<Honey> printHoneyWriter() {
        return honeys -> {
            honeys.forEach(honey -> log.info(honey.toString()));
        };
    }
}
