package com.hidepiy.batch.job.reader;

import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.support.ListItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hidepiy.batch.domain.dto.Honey;

@Configuration
public class HoneyReader {

    @Bean
    public ItemReader<Honey> lightHoneyListReader() {
        List<Honey> honeyList = new ArrayList<>();
        honeyList.add(new Honey("Audrey"));
        honeyList.add(new Honey("Kitty"));
        honeyList.add(new Honey("Mimi"));
        return new ListItemReader<>(honeyList);
    }
    
    @Bean
    public ItemReader<Honey> heavyHoneyListReader() {
        List<Honey> honeyList = new ArrayList<>();
        honeyList.add(new Honey("Bianca"));
        honeyList.add(new Honey("Flora"));
        return new ListItemReader<>(honeyList);
    }
    
}
